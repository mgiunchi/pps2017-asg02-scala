package com.mgiunchi.scala.file

import com.mgiunchi.scala.filesystem.FilesystemException

import scala.annotation.tailrec

class Directory(override val parentPath: String, override val name:String, val contents: List[DirEntry])
  extends DirEntry (parentPath, name) {

  def hasEntry(name: String): Boolean = {
    findEntry(name)!=null
  }

  def getAllFoldersInPath : List[String] = {
    // /a/b/c/d => List["a","b","c","d"]
    path.substring(1).split(Directory.SEPARATOR).toList.filter(x => !x.isEmpty)
  }

  def findDescendant(path : List[String]) : Directory = {
    if (path.isEmpty) this //nothing to search
    else findEntry(path.head).asDirectory.findDescendant(path.tail)
  }

  def addEntry(newEntry: DirEntry):Directory = {
    new Directory(parentPath, name,contents :+ newEntry)
  }

  def findEntry(entryName: String): DirEntry = {
    @tailrec
    def findEntryHelper(name: String, contentList : List[DirEntry]): DirEntry = {
      if (contentList.isEmpty) null
      else if (contentList.head.name.equals(name)) contentList.head
      else findEntryHelper(name, contentList.tail)
    }
    findEntryHelper(entryName,contents)
  }

  def replaceEntry(entryName: String, newEntry: DirEntry):Directory = {
    //keeps every entry in content list which does not
    //have the name entryName
    new Directory(parentPath,name,contents.
      filter(x => !x.name.equals(entryName)) :+newEntry)
  }

  def isRoot(): Boolean = parentPath.isEmpty

  override def asDirectory: Directory = this

  override def asFile: File = throw new FilesystemException("A directory can't be converted to a file")

  override def isDirectory: Boolean = true

  override def isFile: Boolean = false

  override def getType: String = "Directory"
}

object Directory {
  val SEPARATOR = "/"
  val ROOT_PATH = "/"

  //create root
  def ROOT()= Directory.empty("","")
  //Create an empty directory
  def empty(parentPath : String , name: String): Directory =
    new Directory(parentPath,name,List())
}
