package com.mgiunchi.scala.functional

abstract class MyList[+A] {

  def head():A
  def tail():MyList[A]
  def isEmpty():Boolean
  def add[B>:A](elem: B): MyList[B] //B is superType of A
  def printElements: String
  //polymorphic call
  override def toString():String = "[" + printElements + "]"

  //Take a func like param and gets a List
  def map[B](transformer: A=>B): MyList[B]
  def flatMap[B](transformer: A=>MyList[B]):MyList[B]
  def filter(predicate: A=>Boolean):MyList[A]

  //concatenate function
  def ++[B >:A](list: MyList[B]):MyList[B]

  //high order function
  def foreach(f: A=>Unit): Unit
  def sort(comp: (A,A)=>Int):MyList[A]
  def zipWith[B,C](list: MyList[B], zipFun: (A, B) => C): MyList[C]
  def fold[B](startValue : B)(operator: (B,A)=>B):B

  def zipRight[B>:A](acc: Int): MyList[(A,Int)]

}
/*
  head = first element
  tail = remainder of the list
  isEmpty = is this list empty
  add(int) => new list with elem added
  toString => a string representation to list
 */
case object Empty extends MyList[Nothing]{
  override def head(): Nothing = throw new NoSuchElementException

  override def tail(): MyList[Nothing] = throw new NoSuchElementException

  override def isEmpty(): Boolean = true

  override def add[B>:Nothing](elem : B): MyList[B] = new Cons(elem, Empty)

  def printElements : String = ""

  def map[B](transformer: Nothing=>B): MyList[B] = Empty
  def flatMap[B](transformer: Nothing=>MyList[B]):MyList[B] = Empty
  def filter(predicate: Nothing=>Boolean) : MyList[Nothing] = Empty

  def ++[B >:Nothing](list: MyList[B]):MyList[B] = list

  override def foreach(f: Nothing => Unit): Unit = ()

  override def sort(comp: (Nothing, Nothing) => Int): MyList[Nothing] = Empty

  override def zipWith[B,C](list: MyList[B], zipFun: (Nothing, B) => C): MyList[C] = {
    if (!list.isEmpty()) throw new RuntimeException("List does not have the same lenght")
    else Empty
  }

  def fold[B](startValue: B)(operator: (B, Nothing) => B): B = startValue

  def zipRight[B >: Nothing](acc: Int): MyList[(Nothing, Int)] = Empty
}

case class Cons[+A](h: A, t: MyList[A]) extends MyList[A] {
  override def head(): A = h

  override def tail(): MyList[A] = t

  override def isEmpty(): Boolean = {
    if (h==null && t.isEmpty()) true
    else false
  }

  def printElements : String = {
    if (!t.isEmpty()) {
      h + " " + t.printElements
    } else h+ " "
  }

  def add[B>:A](elem: B): MyList[B] = {
    new Cons(elem,this)
  }

  def map[B](transformer: A=>B): MyList[B] = {
    new Cons(transformer(h),t.map(transformer))
  }
  def flatMap[B](transformer: A=>MyList[B]):MyList[B] = {
    transformer(h).++(t.flatMap(transformer))
  }
  def filter(predicate: A=>Boolean):MyList[A] = {
    if (predicate(h)) new Cons(h,t.filter(predicate))
    else t.filter(predicate) //head is filtered
  }
  def ++[B >:A](list: MyList[B]):MyList[B] = {
    new Cons(h,t.++(list))
  }

  // [1,2,3].foreach(x=>println(x))
  def foreach(f: A=>Unit): Unit = {
    f(head)
    t.foreach(f)
  }
  // [1,2,3].sorted((x,y)=>x-y
  def sort(comp: (A, A) => Int): MyList[A] = {
    def insert(x:A,sortedList : MyList[A]):MyList[A]= {
      if (sortedList.isEmpty()) new Cons(x,Empty)
      else if(comp(x,sortedList.head) <= 0) new Cons(x,sortedList)
      else new Cons(sortedList.head,insert(x,sortedList.tail))
    }
    val sortedTail = tail.sort(comp)
    insert(head,sortedTail)
  }

  //[1,2,3]zipwith((x,y)=>x+y) =>
  def zipWith[B,C](list: MyList[B], zipFun: (A, B) => C): MyList[C] = {
    if (list.isEmpty()) throw new RuntimeException("List doesn't have the same lenght")
    else new Cons(zipFun(head,list.head()),tail.zipWith(list.tail,zipFun))
  }

  /*
  [1,2,3]fold(0)(+) =>0+1+2+3
   */
  def fold[B](startValue: B)(func: (B, A) => B): B = {
    val newStartValue = func(startValue,head)
    t.fold(newStartValue)(func)
  }
  //[a,b,c]zipRight(0) =>[(a,0),(b,1),(c,2]
  def zipRight[B>:A](acc: Int): MyList[(A, Int)] = {
    new Cons((h,acc),tail.zipRight(acc+1))
  }
}

/*
trait MyPredicate[-T]{
  def test(elem:T): Boolean
}

//A is controvariant
trait MyTransformer[-A, B]{
  def transform(elem: A): B
}
*/
object ListTest extends App {
  /*
  var list = new Cons(1,new Cons(3,Empty))
  println(list.head())
  println(list.add(2).head)
  var list2 = list.add(2)
  println(list.tail.head)
  println(list2.toString)*/
  var listOfInt = new Cons(1,new Cons(2,new Cons(3,new Cons(4,Empty))))
  var listOfIn2 = listOfInt.copy()
  var listofString = new Cons("a",new Cons("b",new Cons("c",new Cons("d",Empty))))
  println(listOfInt.map(elem=>elem+1))
  println(listOfInt.filter(elem=>elem%2==0))
  println(listOfInt.flatMap(elem=> new Cons(elem, new Cons(elem * 3, Empty))))
  listOfInt.foreach(x=> print(x + " "))
  println()
  println("Sorting")
  println(listOfInt.sort((x,y)=>y-x))
  println(listOfInt.sort((x,y)=>x-y))
  println("ZipWith")
  println(listofString.zipWith[Int,String](listOfInt,(x:String,y:Int)=>x+y))
  println("Fold")
  println(listOfInt.fold(0)((x,y)=> x + y))
  println("ZipRight")
  println(listOfInt.zipRight(0))
  println(listofString.zipRight(0))

  println("Combination")
  //for comprehension
  val combination = for {
    n <- listOfInt
    str <- listofString
  } yield n+"-"+str
  println(combination)

}
