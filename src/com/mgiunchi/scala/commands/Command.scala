package com.mgiunchi.scala.commands

import com.mgiunchi.scala.filesystem.State

/*
* Commands that change state
 */
trait Command {
  def apply(state:State) : State
}

//here we create all commands
object Command {

  val MKDIR  = "mkdir"
  val LS = "ls"
  val PWD = "pwd"
  val TOUCH = "touch"
  val CD = "cd"

  def emptyCommand : Command = new Command {
    override def apply(state: State): State = {
      state
    }
  }

  def incompleteCommand(name : String) : Command = new Command {
    override def apply(state: State): State = {
      state.setMessage(name + ": incomplete command ")
    }
  }

  def from(input: String) : Command = {

    val token : Array[String] = input.split(" ")
    if (input.isEmpty || token.isEmpty) emptyCommand
    else if (MKDIR.equals(token(0))) {
      if (token.length < 2) incompleteCommand(MKDIR)
      //take argument of command
      else new Mkdir(token(1))
    } else if (LS.equals(token(0))) {
        new Ls
    } else if (PWD.equals(token(0))){
        new PrintWorkingDir
    } else if (TOUCH.equals(token(0))){
      if (token.length < 2) incompleteCommand(TOUCH)
      else new Touch(token(1))
    } else if (CD.equals(token(0))){
      if (token.length < 2) incompleteCommand(CD)
      else new Cd(token(1))
    }
    else new UnknownCommand
  }
}