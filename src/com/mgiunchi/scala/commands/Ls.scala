package com.mgiunchi.scala.commands
import com.mgiunchi.scala.file.DirEntry
import com.mgiunchi.scala.filesystem.State

class Ls extends Command{

  def createOutput(contents: List[DirEntry]) : String= {
    if (contents.isEmpty) ""
    else {
      val entry = contents.head
      entry.name +"[" + entry.getType + "]" + "\n" + createOutput(contents.tail)
    }
  }

  //print content of current direc
  override def apply(state: State): State = {
    val contents = state.workingDir.contents
    val output = createOutput(contents)
    state.setMessage(output)
  }
}
