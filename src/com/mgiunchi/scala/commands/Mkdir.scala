package com.mgiunchi.scala.commands
import com.mgiunchi.scala.file.{DirEntry, Directory}
import com.mgiunchi.scala.filesystem.State

class Mkdir(dirName: String) extends CreateEntry(dirName: String){

  override def createSpecificEntry(state: State): DirEntry = {
    Directory.empty(state.workingDir.path,dirName)
  }
}
