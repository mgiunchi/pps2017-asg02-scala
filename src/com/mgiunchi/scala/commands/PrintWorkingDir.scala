package com.mgiunchi.scala.commands
import com.mgiunchi.scala.filesystem.State

class PrintWorkingDir extends Command{

  override def apply(state: State): State = {
    state.setMessage(state.workingDir.path)
  }
}
