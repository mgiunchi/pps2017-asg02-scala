package com.mgiunchi.scala.commands
import com.mgiunchi.scala.filesystem.State

class UnknownCommand extends Command{

  override def apply(state: State): State = {
    state.setMessage("Command not found!")
  }
}
