package com.mgiunchi.scala.commands
import com.mgiunchi.scala.file.{DirEntry, Directory}
import com.mgiunchi.scala.filesystem.State

import scala.annotation.tailrec

class Cd(dir : String) extends Command {

  override def apply(state: State): State = {
    /*
    cd /something/.../dir ->absolute go to dir
    cd a/b/c ->relative go to wd/a/b/c
     */
    //find root
    val root = state.root
    val workingDir = state.workingDir
    //find absolute path or the folder of the dir
    val absolutePath =
      if (dir.startsWith(Directory.SEPARATOR)) dir
      else if(workingDir.isRoot()) workingDir.path + dir
      else workingDir.path + Directory.SEPARATOR + dir
    //find the dir to change to, given the state
    val destinationDir = doFindEntry(root, absolutePath)
    //change the state
    if (destinationDir == null || !destinationDir.isDirectory)
      state.setMessage(dir + ": no such directory")
    else
      State(root,destinationDir.asDirectory)
  }
  def doFindEntry(root: Directory, path: String): DirEntry = {
    @tailrec
    def findEntryHelper(currentDir : Directory, path: List[String]):DirEntry = {
      if (path.isEmpty || path.head.isEmpty) currentDir
        //path empty, no more tokens to explore
      else if(path.tail.isEmpty) currentDir.findEntry(path.head)
      else {
        val nextDir = currentDir.findEntry(path.head)
        if (nextDir == null || !nextDir.isDirectory) null
        else findEntryHelper(nextDir.asDirectory,path.tail)
      }
    }

    @tailrec
    def collapseRelativeTokens(path:List[String], result: List[String]): List[String] = {
      /*
      /a/b = ["a","b"] => ["a","b"]
      /a/.. ="["a",".."] => [] (emptyList=
      /a/b/.. =["a","b",".."] => ["a"]
       */
      if (path.isEmpty) result
      else if(".".equals(path.head)) collapseRelativeTokens(path.tail,result)
      else if ("..".equals(path.head)) {
        if (result.isEmpty) null
        else collapseRelativeTokens(path.tail, result.init) //init= list without first elem
      } else collapseRelativeTokens(path.tail,result :+path.head)
    }
    //tokens
    val tokens : List[String] = path.substring(1).split(Directory.SEPARATOR).toList

    //eliminate/collapse relative tokens
    // /a/../=>["a",".."] =>[]
    // /a/b/../=>["a","b",".."] =>["a"]
    //navigate to the correct entry

    val newTokens = collapseRelativeTokens(tokens,List())
    //navigate to the desired entry
    if (newTokens ==null) null
    else findEntryHelper(root,newTokens)
  }
}
