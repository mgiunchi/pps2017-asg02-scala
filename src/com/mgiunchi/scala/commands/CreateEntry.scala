package com.mgiunchi.scala.commands
import com.mgiunchi.scala.file.{DirEntry, Directory}
import com.mgiunchi.scala.filesystem.State

abstract class CreateEntry(entryName:String) extends Command {

  override def apply(state: State): State = {
    val workingDir = state.workingDir
    //if wd already has entry with this name
    if(workingDir.hasEntry(entryName)){
      state.setMessage("Entry "+entryName+ " already exists!")
    } else if (entryName.contains(Directory.SEPARATOR)){
      state.setMessage(entryName+ " mus not contain separators")
    } else if (checkIllegal(entryName)){
      state.setMessage(entryName + ": illegal entry name")
    } else {
      doCreateEntry(state,entryName)
    }
  }

  def checkIllegal(name: String) : Boolean = {
    name.contains(".")
  }

  def doCreateEntry(state: State, str: String) : State = {
    def updateStructure(currentDirectory: Directory, path: List[String], newEntry: DirEntry ) : Directory = {
      if(path.isEmpty) currentDirectory.addEntry(newEntry)
      else {
        ///a/b => ["a","b"]
        //  (content)
        // new entry /e
        //updateStructure(root,["a","b"],/e)
        val oldEntry = currentDirectory.findEntry(path.head).asDirectory
        currentDirectory.replaceEntry(oldEntry.name,updateStructure(oldEntry,path.tail,newEntry))
      }
    }
    val workingDir = state.workingDir

    //get all the directories in the full path
    val allDirInPath = workingDir.getAllFoldersInPath
    //create new direc entry in working directory
    val newEntry = createSpecificEntry(state)
    //upadte whole directory struc starting from root
    val newRoot = updateStructure(state.root,allDirInPath, newEntry)
    //find new working dir instance given wd's full path in the new dir structure
    val newWorkingDir = newRoot.findDescendant(allDirInPath)
    State(newRoot, newWorkingDir)

  }

  def createSpecificEntry(state: State) : DirEntry
}
