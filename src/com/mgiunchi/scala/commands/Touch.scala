package com.mgiunchi.scala.commands
import com.mgiunchi.scala.file.{DirEntry, File}
import com.mgiunchi.scala.filesystem.State

class Touch(fileName:String) extends CreateEntry(fileName: String) {

  override def createSpecificEntry(state: State): DirEntry = {
    File.empty(state.workingDir.path,fileName)
  }
}
