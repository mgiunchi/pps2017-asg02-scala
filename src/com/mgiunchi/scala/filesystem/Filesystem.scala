package com.mgiunchi.scala.filesystem

import java.util.Scanner

import com.mgiunchi.scala.commands.Command
import com.mgiunchi.scala.file.Directory

object Filesystem extends App{

  //create an empty dir
  val root = Directory.ROOT
  //create a first new state with root dir
  var state = State(root,root)
  val scanner = new Scanner(System.in)

  while(true) {
    state.show //print state of previous command
    val input = scanner.nextLine()
    //creates a command from whatever the user input
    val newCommand = Command.from(input)
    //get new state from new command
    state = newCommand.apply(state)
//    println("$")
//    println(scanner.nextLine())
  }
}
