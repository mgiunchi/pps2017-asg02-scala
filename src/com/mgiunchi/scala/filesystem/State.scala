package com.mgiunchi.scala.filesystem

import com.mgiunchi.scala.file.Directory

/*
* Receives some directories
 */
class State(val root: Directory, val workingDir: Directory,
            val output: String) {
  def show(): Unit = {
    println(output)
    print(State.SHELL_TOKEN)
  }

  //create a new state with apply (with same root, same wd)
  //message will be displayed in console
  def setMessage(message : String): State =
    State(root,workingDir,message)
}

object State {
  val SHELL_TOKEN = "$ "

  //factory method
  def apply(root: Directory, workingDir: Directory,
            output: String=""): State = {
    new State(root,workingDir,output)
  }
}